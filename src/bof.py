#!/usr/bin/python3
# -*- coding: utf-8 -*-
#  Copyright 2016 The University of Sydney
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#
#  bof.py
#  Authors: Jan P Buchmann    <lejosh@members.fsf.org>
#            Mathieu Fourment <mathieu.fourment@uts.edu.au>
#
#  Acknowledgements: Hans Buchmann <hans.buchmann@fhnw.ch>
#                    for suggesting base64
#  Description:
#   The basic bof class handling adding, compression and decompression of bof
#   objects.

import io
import sys
import json
import zlib
import base64


class BofObject:
  """
  Base class for bof objects. The attributes are stored in a map/dictionary
  mimicking a JSON structure.
  """
  def __init__(self):
    self.data = {}

  def add_attribute(self, attrib_name, attrib_value):
    if attrib_name in self.data:
      sys.exit("Bof object::attributeError: {0} already exists. Abort".format(attrib_name))
    self.data[attrib_name] = attrib_value

  def get_attribute(self, attrib_name):
    return self.data[attrib_name]

  def export(self):
    return {x:self.data[x] for x in self.data}

class Bof:
  """
    The base class for bof blocks. It contains two nested classes, Header and
    Compressor. This class has a putative virutal function convert to
    accomodate different file formats.
  """
  class Header:
    """
      Minimal Bof header and required attributes. Requires a  map/dict for
      all expected attributes which value is either 0 (not compressed) or 1
      (compressed). Mandatory header attributes are size and attributes.
      The putative virtual function parse allows implementing specific parsing
      routines.
    """
    def __init__(self, attribs={}):
      self.size = 0
      self.data = attribs

    def export(self):
      return {
               'size' : self.size,
               'data' : {x:self.data[x] for x in self.data}
              }

    def parse(self, headerobj):
      header = json.loads(headerobj)
      self.size = header.pop('size')
      self.data = header.pop('data')

  class Compressor:
    """
      Basic data de/compressor and base64 de/encoder. Does what it says.
    """
    def __init__(self):
      self.level = 6

    def compress(self, data, lvl=0):
      if lvl < 1:
        lvl = self.level
      return base64.b64encode(zlib.compress(data.encode(), lvl)).decode()

    def expand(self, data):
      return zlib.decompress(base64.b64decode(data)).decode()

  def __init__(self, data={}):
    self.header = Bof.Header(data)
    self.compressor = self.Compressor()
    self.payload = []
    self.step_size = 10000
    self.sep = (',', ':')

  def compress(self, bo=None):
    for i in self.payload:
      self.compress_single(i)

  def compress_single(self, bo):
    for i in bo.data:
      if i in self.header.data and self.header.data[i] == 1:
        bo.data[i] = self.compress_data(str(bo.data[i]))

  def compress_data(self, data):
    return self.compressor.compress(data)

  def expand(self):
    for i in self.payload:
      self.expand_single(i)

  def expand_single(self, bo):
    for i in bo.data:
      if i in self.header.data and self.header.data[i] == 1:
        bo.data[i] = self.expand_data(bo.data[i])

  def expand_data(self, data):
    return self.compressor.expand(data)

  def add(self, bo):
    for i in bo.data:
      if i not in self.header.data:
        print("Warning: Unknown data attribute {0}. Skipping.".format(i), file=sys.stderr)
      else:
        if  self.header.data[i] == 1:
          bo.data[i] = self.compress_data(str(bo.data[i]))
    if len(bo.data) == 0:
      print("Warning: BO without attributes. Not adding to payload.", file=sys.stderr)
    else:
      self.payload.append(bo)
      self.header.size += 1
      if self.header.size % self.step_size == 0:
        print("\rbof::objects: added {0}".format(self.header.size), end='',
              file=sys.stderr)

  def export_subset(self, entry_idx):
    subset = Bof()
    subset.header = self.header
    subset.payload = [self.payload[x] for x in entry_idx]
    subset.header.size = len(subset.payload)
    return subset

  def export(self, pretty_print=False):
    indent = None
    objsep = ','
    if pretty_print == True:
      indent = 1
      objsep = ',\n'
    obj_count = 0
    print(json.dumps(self.header.export(), indent=indent, separators=self.sep),
          end='[')
    for i in self.payload:
      obj_count += 1
      if obj_count == self.header.size:
        objsep = ']'
      print(json.dumps(i.export(), indent=indent, separators=self.sep),
            end=objsep)
    print("\nbof::objects: {0} exported ".format(obj_count), file=sys.stderr)

  def write(self):
    obj_count = 0
    end = ','
    for i in self.payload:
      stream = io.StringIO()
      if obj_count == 0:
        print(json.dumps(self.header.export(), separators=self.sep), end='[',
              file=stream)
      if obj_count == self.header.size - 1:
        end = ']'
      print(json.dumps(i.export(), separators=self.sep), end=end, file=stream)
      obj_count += 1
      yield stream.getvalue()

  def convert(self):
    pass
