\documentclass{bioinfo}
\usepackage{url}
\copyrightyear{2015} \pubyear{2015}

\access{Advance Access Publication Date: Day Month Year}
\appnotes{Manuscript Category}

\begin{document}
\firstpage{1}

\subtitle{Data and text mining}

\title[BOF]{BOF: The Biological Object Format}
\author[Buchmann \textit{et~al}.]{Jan P. Buchmann\,$^{\text{\sfb 1,}\#}$, Mathieu Fourment\,$^{\text{\sfb 2,}\#}$ and Edward C. Holmes\,$^{\text{\sfb 1,}*}$}
\address{$^{\text{\sf 1}}$Marie Bashir Institute for Infectious Diseases and Biosecurity, Charles Perkins Centre, School of Life and Environmental Sciences and Sydney Medical School, the University of Sydney, Sydney, New South Wales 2006, Australia and \\
$^{\text{\sf 2}}$ithree Institute, University of Technology Sydney, Ultimo, New South Wales, 2007, Australia.}


\corresp{$^{\#}$These authors contributed equally \\
         $^\ast$To whom correspondence should be addressed.}

\history{Received on XXXXX; revised on XXXXX; accepted on XXXXX}

\editor{Associate Editor: XXXXXXX}

\abstract{\textbf{Motivation:} The large sizes and high complexity of
biological data can represent a major methodological challenge for the analysis
and exchange of large and complex data sets between computers and applications.
In addition, there has been a substantial increase in the amount of metadata
needed to describe biological data, which is beingincreasingly incorporated
into existing data formats. Despite the existence of structured formats based
on XML, biological data sets are mainly formatted using unstructured file
formats and the incorporation of metadata result in increasingly complex
parsing routines and therefore error prone.\\
\textbf{Results:} We present the "biological object format" (BOF), a new format
to exchange and parse nearly all biological data sets more efficiently and with
less error than other currently available formats. Based on JavaScript Object
Notation (JSON) it simplifies parsing by clearly separating the biological data
from its metadata and reduces complexity compared to XML based formats The
ability to selectively compress data up to 87\% compared to currently used file
formats and the reduced complexity result in improved transfer times and less
error prone applications.\\
\textbf{Availability:} BOF was implemented in Python 3.5. The BOF
implementation, documentation and examples are freely available at
https://gitlab.com/virusevolution/bof.git and licensed under the General Public
License version 3.\\
\textbf{Contact:}\href{edward.holmes@sydney.edu.au}{edward.holmes@sydney.edu.au}\\
\textbf{Supplementary information:} Supplementary data are available at \textit{Bioinformatics} online.}
\maketitle

\section{Introduction}
Biological data, which includes, but is not limited to molecular sequences,
annotations, phylogenetic trees, are still predominantly exchanged as flat
files or in line-based formats despite the existence of more structured file
notations that are better suited to complex data. Here, we propose an improved
format for biological data. This new "Biological Object Format" (BOF) is based
on the JSON notation and can support all required data types in biology.

Since the emergence of the first successful format for biological sequences,
the FASTA format in 1985 \citep{Lipman1985}, different formats for a variety of
biological data types have been developed, such as molecular sequences (FASTQ
\citep{Cock2010}), phylogenetic trees (Newick \citep{Felsenstein1989},  NEXUS
\citep{Maddison1997}), and sequence alignments (Stockholm,
\citep{Sonnhammer1997}, BAM \citep{Li2009}). These formats are flat files or
block-based and hence inflexible to modification, complicating the addition of
extra information. Further, each format requires a different parser that has to
be developed and maintained. Although unstructured flat files are very commonly
used for biological data sets, no new approaches have been proposed to simplify
the exchange of increasingly complex sequence data sets.

The "Extensible Markup Language" (XML) has been adopted as a mark-up language
for the exchange of structured biological data, including TinySeq (NCBI),
INSDSEQ (\url{http://www.insdc.org/documents}), NeXML \citep{Vos2012} or
phyloXML \citep{Han2009}. However, XML is complex compared to JSON
(Supplementary Material Note 1), and requires at least four parts: the DTD
schema language (\url{https://www.w3.org/TR/xmlschema-1/}), XSD schema language
(\url{https://www.w3.org/TR/xmlschema11-1/}), XSLT transformation language
(\url{https://www.w3.org/TR/xslt20/}), and XQuery and XPath
(\url{https://www.w3.org/TR/xpath-datamodel-31/}). In addition, XML documents
tend to have a high degree of "noise", as the syntax can use more space than
the information itself. The JSON notation is able to encode the same data as
XML, but with cleaner syntax and less clutter. The reduced complexity needed to
parse data sets results in simpler code and ultimately fewer errors.

Biological data repositories such as the European Molecular Biology Laboratory
(EMBL, \url{https://www.embl.org/}) and the National Center for Biotechnology
Information (NCBI, \url{https://www.ncbi.nlm.nih.gov/}) offer JSON as a data
format within several "Representational state transfer" (REST) application
programming interfaces (APIs). However, JSON is not used for biological data.
For example, NCBI’s Entrez utility or REST API only export biological data in
FASTA and XML formats, although other information is available in
JSON\citep{Kans2016}. The JSON notation is widely used, and the existence of
JSON parsers in almost all programming languages facilitates adapting existent
and creating new pipelines. Herein, we use JSON in our new BOF format, the
structured form of which allows quick and easy adjustments of existing formats
and simplifies the design of new formats.

%\enlargethispage{12pt}

\begin{methods}
\section{Methods}

\textbf{Data set preparation}\\
All data sets were obtained from NCBI (\url{https://www.ncbi.nlm.nih.gov}).
FASTQ data sets were downloaded and created using the NCBI SRA (Short Read
Archive) toolkit version 2.8.2 (https://github.com/ncbi/sra-tools). XML data
sets in the TinySeq format were downloaded from NCBI using the Entrez REST API
\citep{Sayers2009}. All accession numbers can be found in the git repository
under "accessions".
\vspace*{1pt}

\noindent\textbf{Genome data set}\\
Genome sequences were assembled from the RefSeq NCBI
database using the GenBank accession numbers in the assembly reports at
\url{ftp://ftp.ncbi.nlm.nih.gov/genomes/refseq/assembly_summary_refseq.txt}.
Only assembled chromosome or genomes were selected.
The genome data set consist of following subsets:
\begin{table}[!h]
  {\begin{tabular}{@{}lll@{}}
  \toprule
  Organism                          & Subset name & GenBank accession \\\midrule
  \textit{Brachypodium distachyon}  & Bdist       & GCA\_000005505.2   \\
  \textit{Drosophila melanogaster}  & Dmela       & GCA\_000001215.4   \\
  \textit{Escherichia coli}         & Ecoli       & GCA\_000005845.2   \\
  \textit{Homo sapiens}             & Hsapi       & GCA\_000001405.24  \\
  \textit{Mus musculus}             & Mmus        & GCA\_000001635.7   \\
  \textit{Saccharomyces cerevisiae} & Scere       & GCA\_001051215.1   \\
  \textit{Zea Mays}                 & Zmays       & GCA\_001644905.1   \\\botrule
  \end{tabular}}
\end{table}

\noindent\textbf{Collection data set}\\
The collection data set comprises three subsets; (i) virus genomes (viruses),
(ii) expressed sequence tags from plants (Plant EST), and (iii) the
\textit{Blumeria graminis} genome scaffold sequences (Bgram scaffolds). Virus
genome sequences were downloaded using the accession derived from the search
term "(viruses [organism] NOT Bacteria [organism]) AND "complete genome"
[title]" in the NCBI RefSeq database. Plant expressed sequence tags (ESTs) were
downloaded using 1,000,000 random accession derived from the first 2,000,000
results for the search term "viridiplantae[organism]" in the NCBI EST database.
The Blumeria graminis genome scaffolds were downloaded directly from NCBI
(GenBank accession GCA\_000151065.3).{}
\vspace*{1pt}

\noindent\textbf{Protein data set}\\
We created two subsets of plant and human protein
sequences. Plant protein sequences were downloaded using search results for the
term "viridiplantae[organism] NOT (putative OR hypothetical OR predicted)" in
the NCBI protein database, which resulted in 627,778 protein sequences. For the
human protein data set the first million reads for the search term "homo
sapiens [organism] NOT (putative OR hypothetical OR predicted)" were
downloaded.
\vspace*{1pt}

\noindent\textbf{FASTQ data sets}\\
To analyze the impact of sequence length and quality on the compression, NCBI
SRA archives were screened for runs with short reads, long reads, and a mixture
of short and long reads. SRA accession numbers ERR1662731, ERR1988801,
SRR390728, SRR5710238 SRR5787977, and SRR5802898 were downloaded using the NCBI
SRA toolkit. From each run the first million FASTQ entries were extracted, with
the exception of run ERR1662731 which comprised 767,298 FASTQ entries.
\vspace*{1pt}

\noindent\textbf{Phylogenetic data set}\\
Phylogenetic data sets were downloaded as Nexus files from Treebase
(\url{https://treebase.org/treebase-web/home.html}) and
converted into NeXML \citep{Vos2012}.
\vspace*{1pt}

\noindent\textbf{Calculating file sizes}\\
For each subset the file sizes were calculated in bytes. XML files were
analysed as coming directly from Entrez and therefore contained line breaks
(\textbackslash \texttt{n}).
\vspace*{1pt}

\noindent\textbf{Software}\\
The BOF implementation, documentation and examples are available at
https://gitlab.com/virusevolution/bof.git. The bof.py class implements the
basic BOF class, while reader.py implements a basic BOF reader. We implemented
BOF converters for the formats analysed which can be found in the converter
directory. This also provides examples on how to include BOF in existing
pipelines and application.
\vspace*{1pt}

\noindent\textbf{XML and JSON parser complexity analysis}\\
cloc 1.70 (\url{https://github.com/AlDanial/cloc}) was used to count the lines of
code and dependent files in the corresponding source files. Files with the
following regular expression pattern were excluded from the analysis:
\texttt{.*test.*|tests|.*doc.*|.*tutorial.*|examples}.
%\vspace*{1pt}

\noindent\textbf{Analysis}\\
The analysis described here was performed using a 64bit Linux Desktop with an
Intel i5-4670 CPU (4 cores at 3.40 GHz) and 32 GB of RAM running Ubuntu 17.04.
All programs were written in Python 3.5.3. XML and JSON data were parsed using
the internal Python modules \texttt{xml.etree.ElementTree} and json,
respectively. The biological data was compressed using the internal Python
modules zlib (compression level 6) and base64. Phylogenetic data was parsed
using Bio.Phylo \citep{Talevich2012}. Figure 2 was prepared using matplotlib
\citep{Hunter2007}.
\end{methods}

\section{Results} We devised a new file format for biological data, denoted
BOF, and tested it on both molecular sequence and phylogenetic data. Nucleotide
and protein sequences use almost all available characters, have different
lengths and contain metadata, and therefore represent good benchmarks for the
compression. To demonstrate the versatility of BOF to handle non-sequence data
we apply it to a series of phylogenetic data. In addition, we wrote
proof-of-concept tools using our BOF implementation to convert NCBI’s TinySeq
XML, FASTQ, and NeXML formats into BOF.

\subsection{BOF structure}
The underlying basic structure of BOF consists of a header (a JSON object),
followed by a JSON array containing JSON objects and is designated as the
"payload" (Figure~\ref{fig:bofstruc}a). A header and the corresponding payload
constitute a BOF block that can be concatenated (Figure~\ref{fig:bofstruc}a).
The payload follows directly after the header and a new BOF block can follow
immediately after the payload (Figure~\ref{fig:bofstruc}a). JSON objects within
a payload can be parsed using specific functions or classes that can be
included as libraries. Biological data sets are steadily increasing in size and
have to be transferred between computers and applications. We therefore
designed BOF with the ability to store compressed data since the cost of the
transfer, such as downloading or streaming data between two applications, is
higher than compressing or decompressing data.

\begin{figure*}[ptb]
\includegraphics{Figure_1.eps}
\caption{Description of BOF. (a) Schematic diagram of the BOF format indicating
the major components of the format. (b) Basic BOF example in JSON syntax. (c)
TinySeq XML and the corresponding BOF notation. The tables on the left indicate
the size of the attributes and the XML tags or JSON keys, respectively. Sizes
are given in bytes. Only those parts shown in red in the listing are included
in the size calculations. The JSON snippet is an example of a payload item and
the sequence attribute is an obvious candidate for data compression.
\label{fig:bofstruc}}
\end{figure*}

The BOF header is a JSON object and contains two mandatory attributes: "size"
and "data" (Figure~\ref{fig:bofstruc}b). The "size" attribute indicates the
number of data objects in the corresponding payload, excluding any nested
objects; i.e. attributes described as objects (see the section on phylogenetic
trees below). The "data" attribute is a JSON object which keys describes the
data present in each payload while its value if it is compressed using either 0
(not compressed) or 1 (compressed)  (Figure~\ref{fig:bofstruc}b). Other
attributes can be added to accommodate different data sets.

A biological object is a JSON object within a JSON array
(Figure~\ref{fig:bofstruc}b). It stores the different attributes as key/value
pairs in JSON notation containing such elements as a nucleotide or protein
sequence, or a phylogenetic tree. The values do not contain newlines and can be
compressed as a zlib string (\url{https://tools.ietf.org/rfc/rfc1950.txt})
encoded in base64 (\url{https://tools.ietf.org/rfc/rfc3548.txt}). We
implemented BOF in Python and wrote a basic BOF reader to read BOF data
streams, allowing the integration into existing pipelines (see Methods).



\subsection{Sequence data in BOF}
We used NCBI’s TinySeq XML format to assess how BOF handles sequence data.
NCBI’s TinySeq format is encoded using XML and demonstrates the typical
additional parsing complexity for an XML file, as the type of sequence is
encoded as an XML tag attribute (Figure~\ref{fig:bofstruc}c). We wrote a
TinySeq-to-BOF converter (see Methods). We designed a BOF equivalent of a
TinySeq entry using the same naming scheme (Figure~\ref{fig:bofstruc}c). The
sequence for each entry was compressed to demonstrate the compression
capability of BOF.

We analyzed two nucleotide sequence data sets, the Genome and Collection data
sets (Figure~\ref{fig:filesizes}a, Supplementary Table 2, see Methods). The
Genome data set contains subsets with a small number of long sequences, while
the Collection data comprises subsets with a greater number of shorter
sequences.

\begin{figure}[tpb]
\includegraphics{Figure_2.eps}
\caption{File size and sequence size comparisons for the data sets analysed
here. The x-axis depicts the data subset. Upper plots indicate the file size
for each subset and the corresponding portion of the data only, indicated by
the lighter color. The lower violin plots show the corresponding maximum and
minimum data sizes analysed within each subset. The "+" indicates the median
for the corresponding data set. File sizes and sample data sizes are indicated
in the legend. (a) The Genome data set comparing the TinySeq XML format. Due to
their small size the Escherichia coli and Saccharomyces cerevisiae genomes are
not plotted, but given in Supplementary Table 1. The original format is TinySeq
XML. (b) The Collection data set comparing the TinySeq XML format. (c) The SRA
data set comparing the FASTQ format. (d) The Protein data set comparing the
TinySeq XML format. (e) The phylogenetic tree ("phylogenetic") data set
comparing the NeXML format.
\label{fig:filesizes}}
\end{figure}

In the Genome data set, the smallest genome was that from \textit{Escherichia
coli} with one sequence of 4.64~Megabasepairs (Mbp) in length. The subset with
the greatest number of sequences was the human genome (a total of 25 sequences
ranging from 0.01~Mbp to 249~Mbp and totalling $\approx$~3,080~Mbp), while the
subset with the longest sequence was the \textit{Zea mays} genome (10 sequences
between 155~Mbp and 318~Mbp, totalling $\approx$~2,180~Mbp).

The Collection data set consist of three sub sets, expressed sequence tags
(ESTs) from plants (Plant EST), RefSeq virus genomes (virus genomes), and
scaffolds from the \textit{Blumeria graminis} genome (Bgram, Supplementary
Table 2). The Plant EST subset contained the most sequences (1,000,000) ranging
between 1 and 4,802~bp and totalling approximately 544,350~Mbp. The Viruses
subset contains 66,725 virus genome sequences ranging between 19~bp and
2,243,109~bp totalling approximately 1.28~Gigabasepairs (Gbp). The Bgram sub
set consists of 6,845 \textit{B.graminis} genome scaffold sequences between
668~bp and 9,686,481~bp, totalling $\approx$~119.73 Mbp

The protein data set has two subsets, Hsapi with 1,000,000 human protein and
Plant with 627,778 plant protein sequences (Supplementary Table 3). Sequences
in the Hsapi sub set range between 1 amino acid (aa) and 35,991~aa in length,
totalling 336,817,042~aa. The Plant sub set sequences range between 1~aa and
21,004~aa, totalling 232,954,947~aa.

The BOF equivalent of a TinySeq XML entry contains the same amount of
information but is more easily parsed and has approximately 13\% less clutter
(Figure~\ref{fig:bofstruc}c). The TinySeq and uncompressed BOF files in the
Genome and Collection data sets were almost identical in size, with the
exception of the Plant EST subset which was approximately 18\% smaller in BOF.
However, the compressed BOF files were between 43\% and 70\% smaller
(Figure~\ref{fig:filesizes}a,b; Supplementary Table 2). Within the protein
dataset, the uncompressed BOF files were approximately 20\% smaller and the
compressed BOF files were approximately 30\% smaller when compared to the
initial TinySeq format (Figure~\ref{fig:filesizes}c; Supplementary Table 3).

We also calculated the data ratio to file size: the higher this ratio, the more
data and less clutter. In data subsets containing a low number of sequences,
such as the Genome data set, this ratio is almost 1 among TinySeq and both
uncompressed and compressed BOF files. However, with increasing sequence
numbers BOF has an improved clutter ratio compared to XML formats
(Figure~\ref{fig:filesizes}, Supplementary Table 3). Due to the compression, a
decrease in the data ratio with increased entries is expected in compressed BOF
data sets.

\subsection{FASTQ data in BOF}
To examine how BOF compares to a non-structured file format we converted FASTQ
files into BOF. We designed the BOF equivalent of a FASTQ entry containing the
read id, spot id, sequence, quality, and sequence length. We compressed the
sequence and quality strings, thereby demonstrating the ability to compress
several attributes (Figure~\ref{fig:filesizes}d; Supplementary Table 4).

FASTQ has emerged as the de facto standard for sequence data, and allows the
storage of both the sequence and the corresponding quality scores. FASTQ
sequence archives are flat files and comprise four parts appearing in sequence
(as described in \citep{Cock2010} and is error prone to parse. It stores
the sequence in FASTA format, therefore allowing line breaks which have to be
considered when parsing. Further, the quality score of each base pair position
is encoded as ASCII decimal code between 33--126. Since the character "@"
(ASCII code 64) indicates a new entry and can also occur at the beginning of
the quality string, a new sequence is not unambiguously indicated. This
requires additional testing to differentiate between sequence and quality
string in FASTQ. In contrast, BOF clearly separates these attributes.

For all uncompressed BOF files we observed an increase in file size between 1\%
and 8\%; this is expected since we add attribute names not present in the FASTQ
specifications (Figure~\ref{fig:filesizes}d, Supplementary Table 4). The
exception was data set SRR5802898 which was approximately 8\% smaller. However,
among compressed BOF files, run ERR1988801 was $\approx$2\% larger, while all
other files were between 6\% and 44\% smaller (Supplementary Table 4). The
anomaly in run ERR1988801 was caused by the attribute "readid" which was a sole
integer and therefore unusually short.

\subsection{Phylogenetic data in BOF}
To demonstrate the versatility of BOF we designed a method to encode
phylogenetic trees using the JSON syntax based on NeXML \citep{Vos2012} and
that allows the addition of arbitrary metadata (Figure~\ref{fig:filesizes}e,
Supplementary Table 5). Metadata is especially important in phylogenetic data
sets. While the XML format is often used for phylogenetic data, such as NeXML
or phyloXML, adding extra metadata complicates parsing. BOF allows the addition
of numerous descriptive attributes while compressing the phylogenetic tree. We
treated each BOF object as one phylogenetic tree in which the mandatory
attribute "tree" contains the phylogenetic tree. The tree itself is encoded in
JSON format as two sets: "vertices" and "edges". This graph structure is
similar to the NeXML\citep{Vos2012} format and to GraphML \citep{Brandes2001} a
general purpose XML graph format. Each edge has three mandatory attributes:
"id", "source" and "target". The "source" and "target" attributes contain
references to two vertices and the "id" is a unique identifier. Each vertex
also contains a unique identifier "id" and a "name" attribute that represents a
taxon name or labelled lineage in the context of a phylogenetic tree. If branch
lengths are given, nodes contain the attribute "branch\_length". Other
metadata, such as an estimate of nucleotide substitution rate and its
confidence interval can be specified within an edge definition. Importantly,
metadata describing the tree that can be investigated while  parsing can be
added as an attribute to the BOF object. This graph structure is flexible as it
is can encode bifurcating trees and more complex graphs such as ancestral
recombination graphs (ARG) that require several additional attributes (e.g.
break-point locations in an sequence alignment, or the location if
recombination events in a phylogeny).

We downloaded three phylogenetic subsets from Treebase \citep{Piel2009} and
compared the NeXML format to both uncompressed and compressed BOF. The first
subset contained 60 trees with 5 to 7 nodes each. The second subset contained
the only five trees but with 144 to 261 nodes. The largest subset contained 541
trees with 26,7281 nodes. BOF reduces the file size between 30\% and 37\%
compared to NeXML, while the compressed BOF files are between 65\% and 87\%
smaller (Figure~\ref{fig:filesizes}e, Supplementary Table 5).

\section{Discussion}
To effectively analyse the ever increasing amount of biological data being
generated it is important that it is efficiently exchanged between both
collaborators and computer applications. With BOF, we describe a new file
format that is tailored to handle these increased data sizes, that enables
adjustments to be made to virtually all biological data sets, and can requires
only a basic knowledge of computer programming.

Not only has the amount of biological data increased, but also its complexity.
As a consequence, it is critical to develop efficient ways to store and analyse
any associated metadata. The increasing necessity to add biological metadata
can be observed in the SRA database at NCBI
(\url{https://www.ncbi.nlm.nih.gov/sra}),
where the length of the sequence is added to the first line of each entry when
using the SRA toolkit. Metadata is especially important in phylogenetic data
sets. However, a recent study demonstrated that the widely used Newick format
is prone to unclear semantics, which can lead to misinterpretation of the data
analysed (Czech, et al., 2017). BOF alleviates these shortcomings by using a
less complex approach than XML formats. Importantly, BOF has not been designed
to be human readable but to write less error-prone tools to exchange, screen or
prepare large data set for analysis. The JSON notation further supports this
approach since it uses structures that are familiar to programmers.

In sum, BOF is a lean file format for almost all biological data based on a
widely used notation and is therefore more stable than other available formats.
BOF enables a clearer structure of biological data which is achieved using the
JSON notation, and allows metadata to be added without increasing complexity.
We demonstrate this versatility with our basic BOF library that can be used for
all data sets by adjusting the basic BOF object without the need to adjust the
underlying parser. We revealed a data compression of up to 70\% on sequence data
and up to 87\% on phylogenetic data, depending on the number and size of the
compressed data. This compression is especially powerful when used with large
data sets containing metadata that are increasingly common in biology.


\section*{Acknowledgements}

We thank Prof. Hans Buchmann for fruitful discussions.
\vspace*{-12pt}

\section*{Funding}

This work was supported by an NHMRC Australia Fellowship to E.C.H (GNT1037231).
\vspace*{-12pt}

\bibliographystyle{natbib}
%\bibliographystyle{achemnat}
%\bibliographystyle{plainnat}
%\bibliographystyle{abbrv}
%\bibliographystyle{bioinformatics}
\bibliography{references}
%
%\bibliographystyle{plain}
%
%\bibliography{Document}


%\begin{thebibliography}{}

%\bibitem[Bofelli {\it et~al}., 2000]{Boffelli03}
%Bofelli,F., Name2, Name3 (2003) Article title, {\it Journal Name}, {\bf 199}, 133-154.

%\bibitem[Bag {\it et~al}., 2001]{Bag01}
%Bag,M., Name2, Name3 (2001) Article title, {\it Journal Name}, {\bf 99}, 33-54.

%\bibitem[Yoo \textit{et~al}., 2003]{Yoo03}
%Yoo,M.S. \textit{et~al}. (2003) Oxidative stress regulated genes
%in nigral dopaminergic neurnol cell: correlation with the known
%pathology in Parkinson's disease. \textit{Brain Res. Mol. Brain
%Res.}, \textbf{110}(Suppl. 1), 76--84.

%\bibitem[Lehmann, 1986]{Leh86}
%Lehmann,E.L. (1986) Chapter title. \textit{Book Title}. Vol.~1, 2nd edn. Springer-Verlag, New York.

%\bibitem[Crenshaw and Jones, 2003]{Cre03}
%Crenshaw, B.,III, and Jones, W.B.,Jr (2003) The future of clinical
%cancer management: one tumor, one chip. \textit{Bioinformatics},
%doi:10.1093/bioinformatics/btn000.

%\bibitem[Auhtor \textit{et~al}. (2000)]{Aut00}
%Auhtor,A.B. \textit{et~al}. (2000) Chapter title. In Smith, A.C.
%(ed.), \textit{Book Title}, 2nd edn. Publisher, Location, Vol. 1, pp.
%???--???.

%\bibitem[Bardet, 1920]{Bar20}
%Bardet, G. (1920) Sur un syndrome d'obesite infantile avec
%polydactylie et retinite pigmentaire (contribution a l'etude des
%formes cliniques de l'obesite hypophysaire). PhD Thesis, name of
%institution, Paris, France.

%\end{thebibliography}
\end{document}
