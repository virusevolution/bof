class TinySeqSequence(BofObject):

  def __init__(self):
    super().__init__()
    self.tagmap = {
                    'TSeq_defline'  : 'defline',
                    'TSeq_seqtype'  : 'typ',
                    'TSeq_gi'       : 'gi',
                    'TSeq_taxid'    : 'taxid',
                    'TSeq_length'   : 'length',
                    'TSeq_orgname'  : 'orgname',
                    'TSeq_sequence' : 'sequence',
                    'TSeq_accver'   : 'accver',
                    'TSeq_sid'      : 'id'
                  }
