#!/bin/bash
#  simplify_tinyseq.sh
#
#  Copyright 2017 The University of Sydney
#  Author: Jan P Buchmann <jan.buchmann@sydney.edu.au>
#  Description:
#   Quick and dirty way to make concatenated NCBI's TinySeq XMLs parseable
#  Version: 0
sed -r '/<?xml version=\"1.0\"/d' $1  | \
sed -r '/!DOCTY/d'                    | \
sed -r '/<*TSeqSet>/d'                | \
(echo "<TSeqSet>" && cat)             | \
(cat  - && echo "</TSeqSet>")
