#!/bin/bash
#  count_code_lines.sh
#
#  Copyright 2017 The University of Sydney
#  Author: Jan P Buchmann <jan.buchmann@sydney.edu.au>
#  Description:
#
#  Version: 0

declare -a libs=("$HOME/downloads/libjsoncpp-1.7.2"
                 "$HOME/downloads/libxml2-2.9.4+dfsg1"
                 "$HOME/downloads/rapidxml-1.13"
                 "$HOME/downloads/xerces-c-3.1.3+debian"
                 "/usr/lib/python3.5/xml"
                 "/usr/lib/python3.5/json")
db=parsers_cloc.db
truncate $db -s 0
count=0
excl="/.*test.*|tests|.*doc.*|.*tutorial.*|examples/g"
for i in ${libs[@]}
  do
    if [[ $count == 0 ]]
      then
        ##cloc $i --report-file="$count.cloc"
        cloc --sql 1 --sql-project $i $i  | sqlite3 $db
      else
        cloc --sql 1 --sql-project $i --sql-append $i  | sqlite3 $db
    fi
    count=$((count+1))
  done
#find . -iname '*.cloc' | xargs cloc --sum_reports --report-file=parser.summary

#cloc --list-file=$files

sqlite3 -header -csv $db  \
'SELECT project, language, count(file), sum(nCode) file
 FROM t
 WHERE (Language="Python" OR Language LIKE "C%" AND Language NOT LIKE "%make%")
  AND ( file NOT LIKE "%test%" AND file NOT LIKE  "%doc%" AND file NOT LIKE "%example%")
GROUP BY
  project,language;'
