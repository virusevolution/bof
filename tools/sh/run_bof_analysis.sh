#!/bin/bash
#  analyze_bof.sh
#
#  Copyright 2017 The University of Sydney
#  Author: Jan P Buchmann <jan.buchmann@sydney.edu.au>
#  Description:
#   Run analysis of bof samples
#  Version: 0

shm="/dev/shm"
db=$2
dbdir="$shm"

function fix_xml()
{
  sed -r '/<?xml version=\"1.0\"/d' $1  | \
  sed -r '/!DOCTY/d'                    | \
  sed -r '/<*TSeqSet>/d'                | \
  (echo "<TSeqSet>" && cat)             | \
  (cat  - && echo "</TSeqSet>") < $1
}

rm -f "$dbdir/$db"
while read -r line
  do IFS="," read fil dset fmt <<< $line;
  fname=$(echo $fil | cut -d'/' -f2)
  cp -v $fil $shm
  memfil="$shm/$fname"
  echo "$fil, $dset, $fmt, $fname, $memfil"
  if [ "$fmt" == 'fq' ]
    then
      ../tools/datasampler/src/ds_analyzer.py -fq -w  --name $dset --db $dbdir/$2 --file $memfil
      rm -v $memfil
  fi
  if [ "$fmt" == 'nexus' ]
    then
      ../tools/datasampler/src/ds_analyzer.py -tree  -w --name $dset --db $dbdir/$2 --file $memfil
      rm -v $memfil
  fi
  if [ "$fmt" == 'xml' ]
    then
      xml_concat="xml.concat"
      ../tools/sh/simplify_tinyseq.sh < $memfil > $shm/$xml_concat
      rm -v $memfil
      ../tools/datasampler/src/ds_analyzer.py -xml -w --name $dset --db $dbdir/$2 --file $shm/$xml_concat
      rm -v $shm/$xml_concat
  fi
  unset IFS;
  done < $1
../tools/datasampler/src/ds_analyzer.py --summarize --db $dbdir/$2
mv -v $dbdir/$db dbs/
