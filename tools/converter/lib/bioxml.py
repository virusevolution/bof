# -*- coding: utf-8 -*-
#  Copyright 2016, 2017 The University of Sydney
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#
#  bioxml.py
#  Author: Jan P Buchmann <lejosh@members.fsf.org>
#  Description:
#   Converting XML sequences into bof
#  Version: 0

import io
import os
import sys
import xml.etree.ElementTree as ET

sys.path.insert(1, os.path.join(sys.path[0], '../../src'))
import bof

class TinySeqSequence(bof.BofObject):

  def __init__(self):
    super().__init__()
    self.tagmap = {
                    'TSeq_defline'  : 'defline',
                    'TSeq_seqtype'  : 'typ',
                    'TSeq_gi'       : 'gi',
                    'TSeq_taxid'    : 'taxid',
                    'TSeq_length'   : 'length',
                    'TSeq_orgname'  : 'orgname',
                    'TSeq_sequence' : 'sequence',
                    'TSeq_accver'   : 'accver',
                    'TSeq_sid'      : 'id'
                  }

class TinySeqConverter(bof.Bof):

  def __init__(self, attributes):
    super().__init__(data=attributes)
    self.src = sys.stdin
    self.isStream = True

  def convert(self, fil=None, s=TinySeqSequence()):
    if fil != None:
      self.src = open(fil, 'r')
      self.isStream = False
    for event, elem in ET.iterparse(self.src, events=["start", "end"]):
      if event == 'start' and elem.tag == 'TSeq':
        s = TinySeqSequence()
      if event == 'end' and elem.tag == 'TSeq':
        for i in s.tagmap:
          s.add_attribute(s.tagmap[i], 0)
        s.tagmap = {}
        self.add(s)
        if self.header.size % 10 == 0:
          print("\r\tXmlConverter::converted {0} entries".format(self.header.size), end='',
            file=sys.stderr)
      if event == 'end' and elem.tag in s.tagmap:
        if elem.tag == 'TSeq_seqtype':
          s.add_attribute(s.tagmap[elem.tag], elem.attrib['value'])
        else:
          s.add_attribute(s.tagmap[elem.tag], elem.text)
        s.tagmap.pop(elem.tag)

    if self.isStream == False:
      self.src.close()

  def run(self, src):
    self.convert(src=io.StringIO(src))
