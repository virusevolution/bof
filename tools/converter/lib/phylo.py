#  Copyright 2017 The University of Sydney
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#
#  nexus.py
#  Authors: Jan P Buchmann   <jan.buchmann@sydney.edu.au>
#           Mathieu Fourment <mathieu.fourment@uts.edu.au>
#  Description:
#   A tree to bof converter

import io
import sys
import os
import json
import hashlib
from Bio import Phylo
from Bio.Phylo import BaseTree

sys.path.insert(1, os.path.join(sys.path[0], '../../src'))
import bof

class PhylogeneticTree(bof.BofObject):

  def __init__(self):
    super().__init__()

class PhyloTreeConverter(bof.Bof):

  def __init__(self, attributes):
    super().__init__(data=attributes)

  def convert(self, fil, fmt='nexml'):
    for i in Phylo.parse(fil, fmt):
      t = PhylogeneticTree()
      t.add_attribute('tree', self.dictionize(i))
      t.add_attribute('leaf_nodes', i.count_terminals())
      t.add_attribute('internal_nodes', len(i.get_nonterminals()))
      t.add_attribute('nodes', i.count_terminals() + len(i.get_nonterminals()))
      t.add_attribute('rooted', i.rooted)
      name = i.name
      if i.name == None:
        name = hashlib.md5(json.dumps(self.dictionize(i)).encode()).hexdigest()
      t.add_attribute('name', name)
      self.add(t)

  def dictionize(self, tree, index_vertex=0, index_edge=0):
    """
    Convert a Tree object to a dictionary using postorder traversal
    :param tree: tree object
    :param index_vertex: starting index for naming vertices
    :param index_edge: starting index for naming edges
    :return:
    """
    graph = dict()
    vertices = []
    edges = []
    vertex_map = {}

    for idx, node in enumerate(tree.find_clades(order='postorder')):
        vertex_id = 'vertex{}'.format(index_vertex)
        vertex = {'id': vertex_id}
        vertex_map[node] = vertex_id

        if node.name:
            vertex['name'] = node.name

        if tree.rooted and node == tree.root:
            vertex['root'] = True

        vertices.append(vertex)
        index_vertex += 1

        if not node.is_terminal():

            for child in node:
                edge = {'id':'edge{}'.format(index_edge), 'source': vertex_id, 'target': vertex_map[child]}
                if child.branch_length:
                    edge['branch_length'] = child.branch_length
                edges.append(edge)
                index_edge += 1

    graph['vertices'] = vertices
    graph['edges'] = edges
    return graph
