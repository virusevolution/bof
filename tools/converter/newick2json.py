import io
import json
#import StringIO

from Bio import Phylo
from Bio.Phylo import BaseTree


def dictionize(node):
    """
    Convert a Tree object to a dictionary using postorder traversal
    :param node: current node
    :return: node as a dict
    """
    d = dict()
    if node.name:
        d['name'] = node.name
    if node.branch_length:
        d['branch_length'] = node.branch_length

    if not node.is_terminal():
        d['clade'] = [dictionize(child) for child in node]
    return d


def dictionize2(tree, index_vertex=0, index_edge=0):
    """
    Convert a Tree object to a dictionary using postorder traversal
    :param tree: tree object
    :param index_vertex: starting index for naming vertices
    :param index_edge: starting index for naming edges
    :return:
    """
    graph = dict()
    vertices = []
    edges = []
    vertex_map = {}

    for idx, node in enumerate(tree.find_clades(order='postorder')):
        vertex_id = 'vertex{}'.format(index_vertex)
        vertex = {'id': vertex_id}
        vertex_map[node] = vertex_id

        if node.name:
            vertex['name'] = node.name

        if tree.rooted and node == tree.root:
            vertex['root'] = True

        vertices.append(vertex)
        index_vertex += 1

        if not node.is_terminal():

            for child in node:
                edge = {'id': 'edge{}'.format(index_edge), 'source': vertex_id, 'target': vertex_map[child]}
                if child.branch_length:
                    edge['branch_length'] = child.branch_length
                edges.append(edge)
                index_edge += 1

    graph['vertices'] = vertices
    graph['edges'] = edges

    return graph


def dict2tree(subdic):
    """
    Convert a dictionary to a tree
    :param subdic: current node represented by a dict
    :return: current node object
    """
    clade = BaseTree.Clade(branch_length=subdic.get('branch_length'), name=subdic.get('name'))
    if 'clade' in subdic:
        clade.clades = [dict2tree(x) for x in subdic['clade']]
    return clade


def dict2tree2(dic):
    """
        Convert a dictionary to a tree
        :param dic: tree as a dictionary containing edges and vertices
        :return: a tree
        """
    vertices_dict = {}
    root = None

    for v in dic['vertices']:
        vertex = BaseTree.Clade(name=v.get('name'))
        vertices_dict[v['id']] = vertex
        if 'root' in v['id'] and v['id']:
            root = vertex

    for edge in dic['edges']:
        source = vertices_dict[edge['source']]
        target = vertices_dict[edge['target']]
        if 'target' in edge:
            target.branch_length = edge['branch_length']
        source.clades.append(target)

    if root is None:
        # We choose the fake root node of an unrooted tree to be the source vertex of the last edge.
        last_vertex = vertices_dict[dic['vertices'][-1]['id']]
        tree = BaseTree.Tree(root=last_vertex)
    else:
        tree = BaseTree.Tree(root=root)

    return tree


# Create tree object
tree = Phylo.read(io.StringIO("(A:1, (B:2, C:3)theclade:4, (D:5, E:6):7)"), "newick")
Phylo.draw_ascii(tree)

# Convert tree to a dict and save as a json string
clade_dict = dictionize(tree.root)
tree_dict = {'tree': clade_dict, 'rooted': tree.rooted}
tree_json_string = json.dumps(tree_dict)
print(tree_json_string)

# Convert json string to tree
tree_dict2 = json.loads(tree_json_string)
root2 = dict2tree(tree_dict2['tree'])
tree2 = BaseTree.Tree(root=root2, rooted=tree_dict2['rooted'])
Phylo.draw_ascii(tree2)

# Check
stream = io.StringIO()
stream2 = io.StringIO()
Phylo.write(tree, stream, 'newick')
Phylo.write(tree2, stream2, 'newick')
assert stream.getvalue() == stream2.getvalue()


print("graphML style")

# Convert tree to a graphML style dict and save as a json string
tree_dict = dictionize2(tree)
tree_json_string = json.dumps(tree_dict)
print(tree_json_string)

# Convert json string to tree
tree_dict2 = json.loads(tree_json_string)
tree2 = dict2tree2(tree_dict2)
Phylo.draw_ascii(tree2)

# Check
stream = io.StringIO()
stream2 = io.StringIO()
Phylo.write(tree, stream, 'newick')
Phylo.write(tree2, stream2, 'newick')
assert stream.getvalue() == stream2.getvalue()


print('rooted tree')
tree = Phylo.read(io.StringIO("(A:1, ((B:2, C:3)theclade:4, (D:5, E:6):7):8)"), rooted=True, format="newick")
print(tree.rooted)
tree_dict = dictionize2(tree)
tree_json_string = json.dumps(tree_dict)
print(tree_json_string)

# Convert json string to tree
tree_dict2 = json.loads(tree_json_string)
tree2 = dict2tree2(tree_dict2)
Phylo.draw_ascii(tree2)

# Check
stream = io.StringIO()
stream2 = io.StringIO()
Phylo.write(tree, stream, 'newick')
Phylo.write(tree2, stream2, 'newick')
assert stream.getvalue() == stream2.getvalue()
