#!/usr/bin/python3
# -*- coding: utf-8 -*-
#
#  bof.parser.py
#
#  Copyright 2016 The University of Sydney
#  Author: Jan P Buchmann <lejosh@members.fsf.org>
#  Description:
#
#  Version: 0


import sys
import io
import os
import json
import argparse

sys.path.insert(1, os.path.join(sys.path[0], '../../../src'))
import bof
import reader

class BofViewer(bof.Bof):

  #class Header(bof.Bof.Header):
    #def __init__(self):
      #super().__init__()

    #def parse(self, obj):
      #header = json.loads(obj)
      #self.size = header['size']
      #self.data = header['data']

  def __init__(self):
    super().__init__()
    self.header = BofViewer.Header()
    self.sep = ':'
    self.indent = None
    self.decomp = True
    self.aoi = {}

  def setup(self, args):
    if len(args.attributes) > 0:
      self.aoi = args.attributes
    self.sep = args.separator
    self.decomp = args.no_decomp

  def parse(self, obj):
    data = json.loads(obj)
    if len(self.aoi) > 0:
      for i in self.aoi:
        if i in self.header.data and self.header.data[i] == 1:
          print(i, self.sep, self.expand_data(data[i]), sep='')
        else:
          print(i, self.sep, data[i], sep='')
    else:
      for i in data:
        if i in self.header.data and self.header.data[i] == 1:
          if self.decomp == True:
            print(i, self.sep, self.expand_data(data[i]), sep='')
          else:
            print(i, self.sep, data[i], sep='')
        else:
          print(i, self.sep, data[i], sep='')

def main():
  ap = argparse.ArgumentParser(description='Simple Bof block viewer')
  ap.add_argument('-a', '--attributes', nargs='*', default={},
                  help='Attributes to parse. Default: Show all')
  ap.add_argument('-n', '--no_decomp', action='store_false',
                  help='No decompression of compressed data.')
  ap.add_argument('-s', '--separator',type=str, default=':',
                  help='Set separator for attribute and value during print')
  ap.add_argument('--header', action='store_true',
                  help='Print header')

  args = ap.parse_args()
  v = BofViewer()
  v.setup(args)
  r = reader.BofReader()
  r.read(v)
  return 0

if __name__ == '__main__':
  main()
