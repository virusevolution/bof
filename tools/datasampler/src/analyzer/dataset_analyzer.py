# -*- coding: utf-8 -*-
#  Copyright 2016, 2017 The University of Sydney
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#
#  analyzer.py
#  Author: Jan P Buchmann <jan.buchmann@sydney.edu.au>
#  Description:
#   Analyzer for the bof data sets
#  Version: 0

import os
import sys
import xml.etree.ElementTree as ET
from Bio import Phylo
from Bio.Phylo import BaseTree

from . import converter
from . import dataset
import database.database

sys.path.insert(1, os.path.join(sys.path[0], '../../../src'))
import bof

sys.path.insert(1, os.path.join(sys.path[0], '../../converter/lib'))
import fastq
import phylo
import bioxml

class DatasetAnalyzer:

  def __init__(self):
    self.write = False
    self.db = database.database.Database()
    self.dset = dataset.Dataset()
    self.name = 'noname_dset'
    self.attributes = {}
    self.bofconverter = converter.BofConverter(self.attributes)

  def get_filesize(self, fil):
    fil.seek(0)
    fil.seek(0, 2)
    return fil.tell()

  def add_to_db(self, db):
    self.db.connect(db)
    dset_rid = self.db.insert_dset((self.dset.name, len(self.dset.samples)))
    for i in self.fmts:
      self.db.insert_fsize((i, self.dset.fsizes[i], dset_rid.lastrowid))

    samples = []
    sample_attribs = {}
    samples_total = 0
    for i in self.dset.samples:
      samples.append((i.name, str(i.id), i.size, dset_rid.lastrowid))
      for j in i.attribute_props:
        if i.id not in sample_attribs:
          sample_attribs[i.id] = []
        sample_attribs[i.id].append((j,
                                     i.attribute_props[j].rawsize,
                                     i.attribute_props[j].compsize,
                                     i.attribute_props[j].shannon,
                                     dset_rid.lastrowid))
      samples_total += 1
      if len(samples) % 100000 == 0:
        self.db.insert_samples(samples, sample_attribs)
        print("\r\tDatasetAnalyzer::added {0} samples".format(samples_total),
              end='', file=sys.stderr)
        samples = []
        sample_attribs = {}
    if len(samples) > 0:
      self.db.insert_samples(samples, sample_attribs)
      print("\r\tDatasetAnalyzer::added {0} samples".format(samples_total),
              end='', file=sys.stderr)
      samples = []
      sample_attribs = {}
    self.db.update_attribsizes()
    print("\n", file=sys.stderr)

  def analyze(self):
    pass

class PhyloTreeAnalyzer(DatasetAnalyzer):

  def __init__(self, name):
    super().__init__()
    self.fmts = ['nexml', 'bof', 'cbof']
    self.dset.add_fmts(self.fmts)
    self.dset.name = name

  def analyze(self, fil):
    print("Start: analyze dataset {0}".format(self.dset.name), file=sys.stderr)
    nexml = fil+".nexml"
    ## Biopython nexus to nexml conversion seems to loose information
    Phylo.convert(fil, "nexus", nexml, "nexml")
    fh = open(nexml, 'r')
    self.dset.fsizes['nexml'] = self.get_filesize(fh)
    fh.close()
    self.bofconverter.convert(phylo.PhyloTreeConverter(attributes=self.attributes),
                              self.dset,
                              nexml,
                              ['name', 'name', 'nodes'],
                              write=self.write)
    #os.remove(nexml)
    print("\nEnd: analysis dataset {0}".format(self.dset.name), file=sys.stderr)

class TinySeqAnalyzer(DatasetAnalyzer):

  def __init__(self, name):
    super().__init__()
    self.fmts = ['xml', 'bof', 'cbof']
    self.dset.add_fmts(self.fmts)
    self.dset.name = name

  def analyze(self, fil):
    print("Start: analyze dataset {0}".format(self.dset.name), file=sys.stderr)
    xml = open(fil, 'r')
    self.dset.fsizes['xml'] = self.get_filesize(xml)
    xml.close()
    self.bofconverter.convert(bioxml.TinySeqConverter(attributes=self.attributes),
                              self.dset,
                              fil,
                              ['accver', 'gi', 'length'],
                              write=self.write)
    print("\nEnd: analysis dataset {0}".format(self.dset.name), file=sys.stderr)

class FastqAnalyzer(DatasetAnalyzer):

  def __init__(self, name):
    super().__init__()
    self.fmts = ['fq', 'bof', 'cbof']
    self.dset.add_fmts(self.fmts)
    self.dset.name = name

  def analyze(self, fil):
    fq = open(fil, 'r')
    self.dset.fsizes['fq'] = self.get_filesize(fq)
    fq.close()
    print("Start: analyze dataset {0}".format(self.dset.name), file=sys.stderr)
    self.bofconverter.convert(fastq.FastqConverter(attributes=self.attributes),
                              self.dset,
                              fil,
                              ['spotid', 'readid', 'length'],
                              write=self.write)
    print("\nEnd: analysis dataset {0}".format(self.dset.name), file=sys.stderr)
