# -*- coding: utf-8 -*-
#
#  converter.py
#
#  Copyright 2017 The University of Sydney
#  Author: Jan P Buchmann <jan.buchmann@sydney.edu.au>
#  Description:
#

import io
import os
import sys

class BofConverter:

  def __init__(self, attributes):
    self.attributes = attributes

  def convert(self, converter, dset, fil,  attribs, write=False):
    print("\tConverting to bof", file=sys.stderr)
    converter.convert(fil=fil)
    self.write(write, dset, 'cbof', converter)
    for i in converter.payload:
      if len(dset.samples) % 10000 == 0:
        print("\r\tAnalyzed {0} objects ({1:.2f})".format(len(dset.samples),
              len(dset.samples)/converter.header.size), end='', file=sys.stderr)
      dset.add_sample(i.data[attribs[0]],
                      str(i.data[attribs[1]]),
                      i.data[attribs[2]],
                      i,
                      converter)
    self.write(write, dset, 'bof', converter)
    print("\r\tAnalyzed {0} objects ({1:.2f})".format(len(dset.samples),
              len(dset.samples)/converter.header.size), end='', file=sys.stderr)

  def write(self, doWrite, dset, suffix, bof):
    if doWrite == True:
      self.write_file(dset, suffix, bof)
    else:
      self.write_mem(dset, suffix, bof)

  def write_file(self, dset, suffix, bof):
    bfh = open('.'.join([dset.name, suffix, str(bof.header.size)]), 'w')
    for i in bof.write():
      bfh.write(i)
    dset.fsizes[suffix] = self.get_fsize(bfh)
    bfh.close()

  def write_mem(self, dset, suffix, bof):
    bof_mem = io.StringIO()
    for i in bof.write():
      bof_mem.write(i)
    dset.fsizes[suffix] = self.get_fsize(bof_mem)
    bof_mem.close()

  def get_fsize(self, fil):
    fil.seek(0, 2)
    return fil.tell()
