#  plotter.py
#
#  Copyright 2017 The University of Sydney
#  Author: Jan P Buchmann <jan.buchmann@sydney.edu.au>
#  Description:
#
#  Version: 0


import sys
import math
import numpy
import matplotlib
import matplotlib.cm as cm
import matplotlib.lines as ml
import matplotlib.pyplot as plt
import matplotlib.gridspec as mgs
import matplotlib.patches as mpatches

import database.database

class Colormap:

  def __init__(self):
    self.colormap = {}

  def prepare(self, values=[], cmap='viridis'):
    cspace = numpy.linspace(0, 1, 255)
    cmap = matplotlib.pyplot.get_cmap(cmap)
    for i in range(len(values)):
      self.colormap[values[i]] = cmap(cspace[int(len(cspace)/len(values) * i)])


class Legend:

  def __init__(self):
    self.exists = {}
    self.artists = []
    self.labels = []

class SeqSizePlotter:

  def __init__(self):
    self.subsample_totsizes = []
    self.medians = []
    self.means = []
    self.xticks_labels = []
    self.xticks_pos = []

  def plot(self, ax, plotgroup, plotorder):
    print("Plotting seqsizes: ", plotgroup.name)
    seqs = []
    for i in plotorder:
      seqs.append([(x / 10**plotgroup.sample_unit_factor) for x in plotgroup.plots[i].samples])
      self.medians.append(numpy.median(seqs[-1]))
      self.subsample_totsizes.append(sum(seqs[-1]))
      self.xticks_labels.append(plotgroup.plots[i].name)
    vplot = ax.violinplot(seqs,
                          self.xticks_pos,
                          showextrema=True,
                          showmedians=False,
                          widths=0.25)

    for vp in vplot['bodies']:
      vp.set_facecolor('#b0aaa7ff')
      vp.set_alpha(1)
    vplot['cmins'].remove()
    vplot['cmaxes'].remove()
    vplot['cbars'].set_edgecolor('#b0aaa7ff')
    #vplot['cbars'].set_alpha(0.75)
    vplot['cbars'].set_linewidth(2)
    vplot['cbars'].set_zorder(1)
    #ax.scatter(self.xticks_pos, self.medians, marker='+', c='#E58F24', s=200, lw=2)
    ax.scatter(self.xticks_pos, self.medians, marker='+', c='black', s=200, lw=2)
    ax.set_ylabel(plotgroup.sample_unit)
    ax.set_ylim(bottom=0)
    ax.set_xlim(left=self.xticks_pos[0]-1.5*self.xticks_pos[0],
                right=self.xticks_pos[-1]+1.5*self.xticks_pos[0])
    ax.set_xticks(self.xticks_pos)
    #ax.set_xticklabels(self.subsample_totsizes)


class FileSizePlotter:

  def __init__(self):
    self.cmapper = Colormap()
    self.legend = Legend()
    self.xticks_pos = []
    self.xtick_labels = []

  def plot(self, ax, plotgroup, plotorder):
    self.cmapper.prepare(plotgroup.fs_order)
    print("Plotting filesizes: ", plotgroup.name)
    xticks_idx = numpy.arange(len(plotgroup.plots))
    idx = 0
    for i in plotorder:
      bar_shift = 0
      for j in plotgroup.fs_order:
        if j in plotgroup.plots[i].fsizes:
          attr = ax.bar(left=xticks_idx[idx]+bar_shift*plotgroup.plots[i].width,
                              width=plotgroup.plots[i].barwidth,
                              height=plotgroup.plots[i].fsizes[j][1] / plotgroup.fsize_factor,
                              color=self.cmapper.colormap[j],
                              alpha=0.75)
          attr_label = j + " data ratio"
          fs = ax.bar(left=xticks_idx[idx]+bar_shift*plotgroup.plots[i].width,
                            width=plotgroup.plots[i].barwidth,
                            bottom=plotgroup.plots[i].fsizes[j][1] / plotgroup.fsize_factor,
                            height=(plotgroup.plots[i].fsizes[j][0]-plotgroup.plots[i].fsizes[j][1]) / plotgroup.fsize_factor,
                            color=self.cmapper.colormap[j],
                            align='center',
                            label=j,
                            alpha=1.0)
          if j not in self.legend.exists:
            self.legend.exists[j] = 1
            self.legend.artists += [fs, attr]
            self.legend.labels  += [j, attr_label]
          bar_shift += 1
      self.xticks_pos.append(xticks_idx[idx]+((bar_shift*plotgroup.plots[i].width)-plotgroup.plots[i].width)/2)
      self.xtick_labels.append(plotgroup.plots[i].name)
      idx += 1

    ax.set_ylabel(plotgroup.fsize_unit, fontsize=1)
    ax.set_xticks(self.xticks_pos)
    ax.set_ylim(bottom=0)
    ax.set_xlim(left=self.xticks_pos[0]-1.5*self.xticks_pos[0],
                right=self.xticks_pos[-1]+1.5*self.xticks_pos[0])
    ax.set_xticklabels(self.xtick_labels)
    ax.set_title(plotgroup.name.title(), y=1.1, fontdict={'fontsize' : 30})
